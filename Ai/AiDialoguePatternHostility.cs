namespace BlamCore.Ai
{
    public enum AiDialoguePatternHostility : short
    {
        None,
        Self,
        Neutral,
        Friend,
        Enemy,
        Traitor
    }
}
