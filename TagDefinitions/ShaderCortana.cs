using BlamCore.Serialization;

namespace BlamCore.TagDefinitions
{
    [TagStructure(Name = "shader_cortana", Tag = "rmct", Size = 0x4)]
    public class ShaderCortana : RenderMethod
    {
        public int Unknown8;    
    }
}