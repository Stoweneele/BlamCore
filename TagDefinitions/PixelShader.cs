using BlamCore.Serialization;
using BlamCore.Shaders;
using System.Collections.Generic;

namespace BlamCore.TagDefinitions
{
    [TagStructure(Name = "pixel_shader", Tag = "pixl", Size = 0x20)]
    public class PixelShader
    {
        public uint Unknown;
        public List<ShaderDrawMode> DrawModes;
        public uint Unknown3;
        public List<ShaderData> Shaders;
    }
}